import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'criminalistica-lgpd';

  ngOnInit(): void {
    sessionStorage.setItem('criminalistica', 'cliente-numero-cartao-de-credito');
    sessionStorage.setItem('criminalistica-lgpd', 'dados sensíveis');
    document.cookie = "username=dadosensivel";
  }
}
